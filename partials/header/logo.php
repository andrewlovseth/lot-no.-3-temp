<div class="site-logo">
	<a href="<?php echo site_url('/'); ?>">
		<img class="no-lazy" src="<?php $image = get_field('logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</a>

	<div class="tagline">
		<img class="no-lazy" src="<?php $image = get_field('tagline', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>
</div>