<?php include(locate_template('partials/header/global-variables.php')); ?>

<div class="social">			
	<a href="<?php the_field('twitter', 'options'); ?>" class="twitter" rel="external">
		<img class="no-lazy" src="<?php echo $child_theme_path; ?>/images/icon-twitter.svg" alt="Twitter" />
	</a>
	
	<a href="<?php the_field('instagram', 'options'); ?>" class="instagram" rel="external">
		<img class="no-lazy" src="<?php echo $child_theme_path; ?>/images/icon-instagram.svg" alt="Instagram" />
	</a>

	<a href="mailto:<?php the_field('email', 'options'); ?>" class="email" rel="external">
		<img class="no-lazy" src="<?php echo $child_theme_path; ?>/images/icon-email.svg" alt="Email" />
	</a>
</div>