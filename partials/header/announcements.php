<?php if(get_field('show_announcements', 'options') == true): ?>

	<section class="announcement">
		<div class="wrapper">
			
			<div class="info">
				<?php the_field('announcements', 'options'); ?>
			</div>

		</div>
	</section>

<?php endif; ?>