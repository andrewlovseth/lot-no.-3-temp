<?php include(locate_template('partials/header/global-variables.php')); ?>

<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo('template_directory'); ?>/heavy-restaurants.css" />
<link rel="stylesheet" href="https://use.typekit.net/gsy2poh.css">
<link rel="stylesheet" type="text/css" media="all" href="<?php echo $child_theme_path; ?>/<?php echo $css_filename; ?>.css?v=1" />