<?php

/*

	Template Name: Home

*/

include(locate_template('partials/header/global-variables.php'));

get_header(); ?>

	<main class="site">

		<section class="hero">

			<img class="hero-image" src="<?php $image = get_field('hero_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<div class="content">
				<div class="logo">
					<div class="wrapper">
						<img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>
				</div>

				<div class="info">

						<div class="status">
							<div class="wrapper">

								<div class="status-box">
									<div class="box-wrapper">
										<div class="headline">
											<h1><?php the_field('headline'); ?></h1>
										</div>

										<div class="copy">
											<?php the_field('copy'); ?>
										</div>											
									</div>
								</div>

							</div>
						</div>

						<div class="contact">
							<div class="wrapper">

								<div class="contact-box">
									<div class="address detail">
										<p><?php the_field('address'); ?></p>
									</div>

									<div class="phone detail">
										<p><?php the_field('phone'); ?></p>
									</div>

									<div class="email detail">
										<p><a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a></p>
									</div>

									<div class="social detail">	
										<a href="<?php the_field('instagram'); ?>" class="instagram" rel="external">
											<img class="no-lazy" src="<?php echo $child_theme_path; ?>/images/icon-instagram.svg" alt="Instagram" />
										</a>

										<a href="<?php the_field('facebook'); ?>" class="facebook" rel="external">
											<img class="no-lazy" src="<?php echo $child_theme_path; ?>/images/icon-facebook.svg" alt="Facebook" />
										</a>

										<a href="<?php the_field('twitter'); ?>" class="twitter" rel="external">
											<img class="no-lazy" src="<?php echo $child_theme_path; ?>/images/icon-twitter.svg" alt="Twitter" />
										</a>
									</div>										
								</div>
								
							</div>
						</div>
					
				</div>
			</div>



		</section>
	
	</main>

<?php get_footer(); ?>