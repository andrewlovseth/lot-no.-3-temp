<?php include(locate_template('partials/header/global-variables.php')); ?>
<!DOCTYPE html>
<html>
<head>
	<?php the_field('head_meta', 'options'); ?>

	<meta charset="utf-8" />
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<?php get_template_part('partials/head/styles'); ?>
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<?php the_field('body_meta', 'options'); ?>

	<?php get_template_part('partials/header/announcements'); ?>