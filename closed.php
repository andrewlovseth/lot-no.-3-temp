<?php

/*
    Template Name: Closed

*/

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">

    <?php get_template_part('partials/head/styles'); ?>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

    <div id="page" class="site">

        <main class="site-content">
            <div class="info">
                <div class="content">
                    <div class="logo">
                        <img src="<?php $image = get_field('logo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                    </div>

                    <div class="copy">
                        <?php the_field('copy'); ?>
                    </div>
                </div>
            </div>

            <div class="photo">
                <img src="<?php $image = get_field('background_photo'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
            </div>
        </main>

    <?php wp_footer(); ?>

    </div>
</body>
</html>